package com.uml.crenndesfoispirbakas.awsome_contact.repository;

import com.uml.crenndesfoispirbakas.awsome_contact.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByLogin(String userName);
}
