package com.uml.crenndesfoispirbakas.awsome_contact.repository;

import com.uml.crenndesfoispirbakas.awsome_contact.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
