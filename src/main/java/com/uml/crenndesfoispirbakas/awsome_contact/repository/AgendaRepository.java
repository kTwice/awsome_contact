package com.uml.crenndesfoispirbakas.awsome_contact.repository;

import com.uml.crenndesfoispirbakas.awsome_contact.model.Agenda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgendaRepository extends JpaRepository<Agenda,Long> {
    Agenda findAgendaByName(String agenda);
}
