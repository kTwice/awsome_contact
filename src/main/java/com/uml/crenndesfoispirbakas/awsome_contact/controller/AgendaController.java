package com.uml.crenndesfoispirbakas.awsome_contact.controller;

import com.uml.crenndesfoispirbakas.awsome_contact.DTO.AgendaDto;
import com.uml.crenndesfoispirbakas.awsome_contact.model.Agenda;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.AgendaRepository;
import com.uml.crenndesfoispirbakas.awsome_contact.service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    @GetMapping("/agenda/{id}")
    public Optional<Agenda> getSingleAgenda(@PathVariable("id") final Long id) {
        return agendaService.getAgendaById(id);
    }

    @GetMapping("/agendas")
    public List<Agenda> getAllAgendas() {
        return agendaService.getAllAgenda();
    }

    @PostMapping("/agenda")
    public ResponseEntity<?> createAgenda(@RequestBody AgendaDto agendaDto) {
        return agendaService.createAgenda(agendaDto);
    }


}
