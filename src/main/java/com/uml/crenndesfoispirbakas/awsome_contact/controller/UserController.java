package com.uml.crenndesfoispirbakas.awsome_contact.controller;

import com.uml.crenndesfoispirbakas.awsome_contact.DTO.UserDto;
import com.uml.crenndesfoispirbakas.awsome_contact.model.User;
import com.uml.crenndesfoispirbakas.awsome_contact.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public Optional<User> getSingleUser(@PathVariable("id") final Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.getAllUser();
    }

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }

}
