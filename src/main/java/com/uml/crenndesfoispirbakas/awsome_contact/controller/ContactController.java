package com.uml.crenndesfoispirbakas.awsome_contact.controller;

import com.uml.crenndesfoispirbakas.awsome_contact.DTO.ContactDTO;
import com.uml.crenndesfoispirbakas.awsome_contact.model.Contact;
import com.uml.crenndesfoispirbakas.awsome_contact.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;

    @GetMapping("/contact/{id}")
    public Optional<Contact> getContactById(@PathVariable("id") final Long id){
        return contactService.getContactById(id);
    }

    @GetMapping("/contact")
    public List<Contact> getContact(){
        return contactService.getContact();
    }

    @PostMapping("/contact")
    public ResponseEntity<?> createContact(@RequestBody ContactDTO contactDTO) {
        return contactService.createContact(contactDTO);
    }

}
