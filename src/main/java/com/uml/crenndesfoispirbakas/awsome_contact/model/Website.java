package com.uml.crenndesfoispirbakas.awsome_contact.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.regex.Pattern;

import static com.uml.crenndesfoispirbakas.awsome_contact.utils.Regex.WEBSITE;

@Data
@AllArgsConstructor
public class Website extends ContactDetail {

    private String url;

    @Override
    public boolean validate() {
        if (url == null || url.isEmpty()) {
            return false;
        }
        else return Pattern.matches(WEBSITE.getRegex(), url);

    }
}
