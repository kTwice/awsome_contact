package com.uml.crenndesfoispirbakas.awsome_contact.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "agenda")
public class Agenda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "contact", nullable = true)
    @OneToMany(
            mappedBy = "agenda",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Contact> contacts;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}
