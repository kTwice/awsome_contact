package com.uml.crenndesfoispirbakas.awsome_contact.model;

import com.uml.crenndesfoispirbakas.awsome_contact.utils.Regex;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.regex.Pattern;

@Data
@AllArgsConstructor
public class Email extends ContactDetail{
    private String email;

    @Override
    public boolean validate() {
        if (email == null || email.isEmpty()) {
            return false;
        }
        else return Pattern.matches(Regex.EMAIL.getRegex(), email);
    }
}
