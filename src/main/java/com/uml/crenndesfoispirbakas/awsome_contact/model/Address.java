package com.uml.crenndesfoispirbakas.awsome_contact.model;

import com.uml.crenndesfoispirbakas.awsome_contact.utils.Regex;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.regex.Pattern;

@Data
@AllArgsConstructor
public class Address extends ContactDetail{
    private String adress;

    @Override
    public boolean validate() {
        if (adress == null || adress.isEmpty()) {
            return false;
        }
        else return Pattern.matches(Regex.ADDRESS.getRegex(), adress);
    }

}
