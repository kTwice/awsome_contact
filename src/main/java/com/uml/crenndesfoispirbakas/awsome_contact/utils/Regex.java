package com.uml.crenndesfoispirbakas.awsome_contact.utils;

public enum Regex {
    PHONE("^\\d{3}-\\d{3}-\\d{4}$"),
    EMAIL("^[A-Za-z0-9+_.-]+@(.+)$"),
    ADDRESS("(.*) ([0-9]{5})$"),
    WEBSITE("^(http|https)://(www.)?([a-zA-Z0-9]+).([a-zA-Z]{2,5})$");

    private String regex;

    Regex(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }
}

