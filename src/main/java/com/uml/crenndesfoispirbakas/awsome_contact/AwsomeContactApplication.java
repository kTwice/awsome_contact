package com.uml.crenndesfoispirbakas.awsome_contact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsomeContactApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsomeContactApplication.class, args);
    }

}
