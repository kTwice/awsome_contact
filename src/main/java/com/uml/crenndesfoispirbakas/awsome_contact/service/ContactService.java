package com.uml.crenndesfoispirbakas.awsome_contact.service;

import com.uml.crenndesfoispirbakas.awsome_contact.DTO.ContactDTO;
import com.uml.crenndesfoispirbakas.awsome_contact.model.*;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.AgendaRepository;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.ContactRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Data
@Service
public class ContactService {

    @Autowired
    public ContactRepository contactRepository;

    @Autowired
    public AgendaRepository agendaRepository;

    public Optional<Contact> getContactById(Long id) {
        return contactRepository.findById(id);
    }

    public List<Contact> getContact() {
        return contactRepository.findAll();
    }

    public ResponseEntity<?> createContact(ContactDTO contactDTO) {
        Contact contact = new Contact();

        Address adress = new Address(contactDTO.getAddress());
        boolean adresseValid = adress.validate();
        if(adresseValid){contact.setAddress(adress.getAdress());}

        Email email = new Email(contactDTO.getEmail());
        boolean emailValid = email.validate();
        if(emailValid){contact.setEmail(email.getEmail());}

        Phone phone = new Phone(contactDTO.getAddress());
        boolean phoneValid = phone.validate();
        if(phoneValid){contact.setAddress(phone.getNumber());}

        Website website = new Website(contactDTO.getAddress());
        boolean websiteValid = website.validate();
        if(websiteValid){contact.setAddress(website.getUrl());}

        Agenda agenda = agendaRepository.findAgendaByName(contactDTO.getAgendaName());
        contact.setAgenda(agenda);
        contact.setName(contactDTO.getName());

        try {
            contactRepository.save(contact);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
