package com.uml.crenndesfoispirbakas.awsome_contact.service;

import com.uml.crenndesfoispirbakas.awsome_contact.DTO.AgendaDto;
import com.uml.crenndesfoispirbakas.awsome_contact.model.Agenda;
import com.uml.crenndesfoispirbakas.awsome_contact.model.User;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.AgendaRepository;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.ContactRepository;
import com.uml.crenndesfoispirbakas.awsome_contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AgendaService {

    @Autowired
    AgendaRepository agendaRepository;

    @Autowired
    UserRepository userRepository;

    public Optional<Agenda> getAgendaById(Long id) {
        return agendaRepository.findById(id);
    }

    public List<Agenda> getAllAgenda() {
        return agendaRepository.findAll();
    }

    public ResponseEntity<?> createAgenda(AgendaDto agendaDto) {
        Agenda agenda = new Agenda();
        agenda.setName(agendaDto.getName());
        User user = userRepository.findUserByLogin(agendaDto.getUserName());
        agenda.setUser(user);
        try {
            agendaRepository.save(agenda);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
