package com.uml.crenndesfoispirbakas.awsome_contact.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uml.crenndesfoispirbakas.awsome_contact.model.Agenda;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Data
@JsonFormat
public class ContactDTO {

    @Nullable
    @JsonProperty("email")
    private String email;

    @Nullable
    @JsonProperty("phone")
    private String phone;

    @Nullable
    @JsonProperty("address")
    private String address;

    @Nullable
    @JsonProperty("website")
    private String website;

    @NotNull
    @JsonProperty("name")
    private String name;

    @NotNull
    @JsonProperty("agendaName")
    private String agendaName;
}
