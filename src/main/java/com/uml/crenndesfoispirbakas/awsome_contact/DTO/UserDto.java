package com.uml.crenndesfoispirbakas.awsome_contact.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class UserDto {

    @NotNull
    @JsonProperty("login")
    private String login;

    @NotNull
    @JsonProperty("password")
    private String password;
}
