package com.uml.crenndesfoispirbakas.awsome_contact.DTO;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uml.crenndesfoispirbakas.awsome_contact.model.User;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class AgendaDto {

    @NotNull
    @JsonProperty("userName")
    private String userName;

    @NotNull
    @JsonProperty("name")
    private String name;
}
