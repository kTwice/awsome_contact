# awsome_contact

##pre-requis
JDK 17 


## diagramme de classe
![awsome_contact.png](awsome_contact.png)

## Getting started

Bien le bonjour erwann sama, tout d'abord pour notre projet il te faut avoir un docker my sql
pour ce faire tu peux lancer la commande:
```
docker run --name awsome-mysql -p 3306:3306 -e MYSQL_USER=java -e MYSQL_PASSWORD=java -e MYSQL_DATABASE=awsome_contact -e MYSQL_ROOT_PASSWORD=root -d mysql
```

Ensuite nous avons du te fournir un .jar de notre application tu peux l'executer en faisant :
```
java -jar [nom-de-notre-fichier]
```

Nous avons enfin du te fournir toute une collection postman sous le format json que tu va pouvoir jouer.
pour ce faire il te suffit d'aller dans import dans postman et d'y glisser le json.

Et enjoy your awsome_contact app (quelques petits kouac encore, mais fonctionne dans l'ensemble)